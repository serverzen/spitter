.. -*-rst-*-

Spitter
=======

Spitter provides a web-based mechanism for updating status messages as well
as following other users.  The primary main page (when logged in) displays
all status messages by all users that are being followed by the
current user.

In other words, Spitter is a Twitter clone.

Project url
  http://src.serverzen.com/spitter
Egg url
  http://pypi.python.org/pypi/Spitter

.. _pyramid: http://pypi.python.org/pypi/pyramid/
