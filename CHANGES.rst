.. -*-rst-*-

Changes
=======

0.4 - Dec 18, 2012
------------------

  * Updated for bitbucket setup

0.3 - Dec 1, 2010
-----------------

  * Changed arguments accepted for the 'runserver' command

  * Added transaction support via repoze.tm2/zope.sqlalchemy

  * Ordering of spits are now done by creation in descending order

0.2.1 - Nov 25, 2010
--------------------

  * Removed use of python2.7's OrderedDict (must run on python2.6)

0.2 - Nov 25, 2010
------------------

  * Added many tests

  * Added ability to follow/unfollow users

  * Improved UI 

0.1 - Nov 23, 2010
------------------

  * Initial version
